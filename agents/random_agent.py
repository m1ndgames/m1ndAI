import random
from typing import List, Union
from agents.base_ml_agent import BaseMLAgent
from numpy.core.multiarray import ndarray


class RandomAgent(BaseMLAgent):
    """
    Random Agent that takes random actions in the game.
    """
    def __init__(self,  state_size, action_size, bot=None):
        """Inherit bot class"""
        super().__init__(state_size, action_size)
        self.bot = bot

    def choose_action(self, state: ndarray, reward: float) -> int:
        return random.choice(self.bot.action_space)

    def on_end(self, state: List[Union[float, int]], reward: float):
        pass
