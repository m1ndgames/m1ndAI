import sc2
from sc2 import Race, Difficulty
from sc2.player import Bot, Computer

from train import TrainAi
bot = Bot(Race.Random, TrainAi())

if __name__ == '__main__':
    sc2.run_game(sc2.maps.get("ReaperVsZealots"), [
        bot,
        Computer(Race.Random, Difficulty.VeryHard)
    ], realtime=False)
