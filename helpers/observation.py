from sc2.units import Units

from sc2.ids.effect_id import EffectId

from sc2.position import Point2, Point3

map_ids = [
    {
        "type_id": 0,
        "name": "OUT OF BOUNDS",
        "icon": "[bold gold3]#[/bold gold3]"
    },
    {
        "type_id": 1,
        "name": "NO VISION",
        "icon": "*"
    },
    {
        "type_id": 2,
        "name": "OWN UNIT",
        "icon": "[bold yellow]o[/bold yellow]"
    },
    {
        "type_id": 3,
        "name": "ALLIED UNIT",
        "icon": "[bold green]a[/bold green]"
    },
    {
        "type_id": 4,
        "name": "ALLIED STRUCTURE",
        "icon": "[bold green]A[/bold green]"
    },
    {
        "type_id": 5,
        "name": "ENEMY UNIT",
        "icon": "[bold red]e[/bold red]"
    },
    {
        "type_id": 6,
        "name": "ENEMY STRUCTURE",
        "icon": "[bold red]E[/bold red]"
    },
    {
        "type_id": 7,
        "name": "BAD EFFECT",
        "icon": "[bold orange]m[/bold orange]"
    },
    {
        "type_id": 8,
        "name": "NOT PATH-ABLE",
        "icon": "+"
    },
    {
        "type_id": 9,
        "name": "NOTHING",
        "icon": " "
    }
]

badeffects = {
    "KD8CHARGE",
    "RAVAGERCORROSIVEBILECP",
    "LURKERMP",
    "BLINDINGCLOUDCP",
    "NUKEPERSISTENT",
    "PSISTORMPERSISTENT",
    "THERMALLANCESFORWARD",
    "LIBERATORTARGETMORPHDELAYPERSISTENT",
    "LIBERATORTARGETMORPHPERSISTENT"
}

class ObservationHandler:
    """General micro_routine for all units
    Includes kiting and shooting routines"""

    def __init__(self, bot=None):
        """Inherit bot class"""
        self.bot = bot
        self.bot.map_info = None

    def create_map(self, unit):
        """
        Saves Map data surrounding a unit
        """
        map_positions = []
        map_info = []
        for x in range(int(unit.position.x) - 14, int(unit.position.x) + 14):
            for y in range(int(unit.position.y) - 14, int(unit.position.y) + 14):
                map_positions.append(Point2((x, y)))

        cell_id = 0
        for position in map_positions:
            added = False
            cell_id += 1

            if not self.bot.in_map_bounds(position):
                occupied_pos = next((item for item in map_info if item["position"] == position), None)
                if not occupied_pos:
                    map_info.append({'position': position, 'type_id': 0, 'cell_id': cell_id})
            elif not self.bot.is_visible(position):
                occupied_pos = next((item for item in map_info if item["position"] == position), None)
                if not occupied_pos:
                    map_info.append({'position': position, 'type_id': 1, 'cell_id': cell_id})
            else:
                rounded_unit_position = Point2((int(unit.position.x), int(unit.position.y)))
                if rounded_unit_position == position:
                    occupied_pos = next((item for item in map_info if item["position"] == position), None)
                    if not occupied_pos:
                        map_info.append({'position': position, 'type_id': 2, 'cell_id': cell_id})
                        continue

                if self.bot.units or self.bot.structures:
                    for our_unit in self.bot.units:
                        if our_unit != unit:
                            rounded_our_unit_position = Point2((int(our_unit.position.x), int(our_unit.position.y)))
                            if rounded_our_unit_position == position:
                                occupied_pos = next((item for item in map_info if item["position"] == position), None)
                                if not occupied_pos:
                                    map_info.append({'position': position, 'type_id': 3, 'cell_id': cell_id})
                                    added = True
                                    continue

                    for our_structure in self.bot.structures:
                        if our_structure != unit:
                            rounded_our_structure_position = Point2((int(our_structure.position.x), int(our_structure.position.y)))
                            if rounded_our_structure_position == position:
                                occupied_pos = next((item for item in map_info if item["position"] == position), None)
                                if not occupied_pos:
                                    map_info.append({'position': position, 'type_id': 4, 'cell_id': cell_id})
                                    added = True
                                    continue

                if self.bot.enemy_units or self.bot.enemy_structures:
                    for enemy_unit in self.bot.enemy_units:
                        rounded_enemy_unit_position = Point2((int(enemy_unit.position.x), int(enemy_unit.position.y)))
                        if rounded_enemy_unit_position == position:
                            occupied_pos = next((item for item in map_info if item["position"] == position), None)
                            if not occupied_pos:
                                map_info.append({'position': position, 'type_id': 5, 'cell_id': cell_id})
                                added = True
                                continue

                    for enemy_structure in self.bot.enemy_structures:
                        rounded_enemy_structure_position = Point2(
                            (int(enemy_structure.position.x), int(enemy_structure.position.y)))
                        if rounded_enemy_structure_position == position:
                            occupied_pos = next((item for item in map_info if item["position"] == position), None)
                            if not occupied_pos:
                                map_info.append({'position': position, 'type_id': 6, 'cell_id': cell_id})
                                added = True
                                continue

                if self.bot.state.effects:
                    for effect in self.bot.state.effects:
                        if effect.id in badeffects:
                            for effect_position in effect.positions:
                                rounded_effect_position = Point2((int(effect_position.x), int(effect_position.y)))
                                if rounded_effect_position == position:
                                    occupied_pos = next((item for item in map_info if item["position"] == position), None)
                                    if not occupied_pos:
                                        map_info.append({'position': position, 'type_id': 7, 'cell_id': cell_id})
                                        added = True
                                        continue

                if not added:
                    if not self.bot.in_pathing_grid(position):
                        map_info.append({'position': position, 'type_id': 8, 'cell_id': cell_id})
                        continue
                    map_info.append({'position': position, 'type_id': 9, 'cell_id': cell_id})
                    continue

        map_info.sort(key=lambda p: (-p["position"][1], p["position"][0]))

        observation_space = []
        for cell in map_info:
            observation_space.append(cell['type_id'])

        if self.bot.train_unit:
            observation_space.append(self.bot.train_unit.type_id.value)
            observation_space.append(self.bot.train_unit.health_percentage)
            observation_space.append(self.bot.train_unit.shield_percentage)
            observation_space.append(self.bot.train_unit.energy)
            observation_space.append(self.bot.train_unit.weapon_cooldown)

            unit_height = self.bot.get_terrain_z_height(Point2(self.bot.train_unit.position))
            observation_space.append(self.bot.train_unit.position.x)
            observation_space.append(self.bot.train_unit.position.y)
            observation_space.append(unit_height)

            enemies: Units = self.bot.all_units.filter(
                lambda u: not u.is_mine)

            if enemies:
                next_enemy = enemies.closest_to(self.bot.train_unit)
                if next_enemy:
                    distance2enemy = next_enemy.distance_to(self.bot.train_unit)
                    if distance2enemy:
                        observation_space.append(distance2enemy)
                    else:
                        observation_space.append(0)

                    observation_space.append(next_enemy.type_id.value)
                else:
                    observation_space.append(0)
                    observation_space.append(0)
            else:
                observation_space.append(0)
                observation_space.append(0)

        else:
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)
            observation_space.append(0)

        if self.bot.print_map:
            self.print_map_cli(map_info)

        episode = self.bot.agent.current_episode
        step = self.bot.agent.ep_steps
        self.bot.table_data = self.bot.table_data + ("[bold]Episode:[/bold]\t\t\t" + str(episode) + "\n\n")
        self.bot.table_data = self.bot.table_data + (
                    "[bold]Global average Reward:[/bold]\t\t" + str(self.bot.agent.avg_reward) + "\n")
        self.bot.table_data = self.bot.table_data + (
                    "[bold]Previous Episode Loss:[/bold]\t\t" + str(self.bot.agent.loss) + "\n\n")

        self.bot.table_data = self.bot.table_data + ("[bold]Entropy:[/bold]\t\t\t" + str(self.bot.agent.entropy) + "\n")
        self.bot.table_data = self.bot.table_data + (
                    "[bold]Policy Loss:[/bold]\t\t\t" + str(self.bot.agent.policy_loss) + "\n")
        self.bot.table_data = self.bot.table_data + (
                    "[bold]Total Loss:[/bold]\t\t\t" + str(self.bot.agent.total_loss) + "\n\n")

        self.bot.table_data = self.bot.table_data + ("[bold]Current Step:[/bold]\t\t\t" + str(step) + "\n")

        # observation_space contains map information as a list of integers
        self.bot.map_info = map_info

        if not self.bot.state_size:
            self.bot.state_size = int(len(observation_space))

        return observation_space

    def print_map_cli(self, map_info):
        """
        Prints a map to the CLI
        """
        cell_previous_y = None
        self.bot.vision_map = ""
        for cell_info in map_info:
            cell = cell_info['position']
            if cell_previous_y != cell.y:
                self.bot.vision_map = self.bot.vision_map + "\n"

            self.bot.vision_map = self.bot.vision_map + (str(map_ids[cell_info['type_id']]['icon']))

            cell_previous_y = cell.y
