from sc2 import AbilityId

from sc2.units import Units
from sc2.position import Point2

action_ids = [
    {
        "id": 0,
        "name": "DONT MOVE"
    },
    {
        "id": 1,
        "name": "MOVE NORTH"
    },
    {
        "id": 2,
        "name": "MOVE NORTH EAST"
    },
    {
        "id": 3,
        "name": "MOVE EAST"
    },
    {
        "id": 4,
        "name": "MOVE SOUTH EAST"
    },
    {
        "id": 5,
        "name": "MOVE SOUTH"
    },
    {
        "id": 6,
        "name": "MOVE SOUTH WEST"
    },
    {
        "id": 7,
        "name": "MOVE WEST"
    },
    {
        "id": 8,
        "name": "MOVE NORTH WEST"
    },
    {
        "id": 9,
        "name": "ATTACK"
    },
    {
        "id": 10,
        "name": "ABILITY"
    }
]

class ActionHandler:
    """General micro_routine for all units
    Includes kiting and shooting routines"""

    def __init__(self, bot=None):
        """Inherit bot class"""
        self.bot = bot
        self.bot.attack_position = None
        self.bot.move_location = None

    def create_actions(self):
        """
        Saves Actions into action_space
        """
        action_space = []
        for action in action_ids:
            action_space.append(action['id'])

        if not self.bot.action_size:
            self.bot.action_size = int(len(action_space))

        return action_space

    async def perform_action(self, action_id):
        self.bot.action = action_ids[action_id]['name']

        enemies: Units = self.bot.all_units.filter(lambda unit: not unit.is_mine)

        if enemies:
            next_enemy = enemies.closest_to(self.bot.train_unit)
            if next_enemy:
                if next_enemy.is_flying:
                    weapon_range = self.bot.train_unit.air_range
                else:
                    weapon_range = self.bot.train_unit.ground_range

                if (self.bot.train_unit.distance_to(next_enemy) - 1) <= weapon_range <= (self.bot.train_unit.distance_to(next_enemy) + 1):
                    self.bot.reward = self.bot.reward + 0.00025

        if action_id == 0:  # Dont Move
            self.bot.table_data = self.bot.table_data + ("[bold]Current Action:[/bold]\t\t\t" + self.bot.action + "\n\n")
            self.bot.do(self.bot.train_unit.move(self.bot.train_unit.position))

        elif 1 <= action_id <= 8:  # Move
            new_position = self.translate_move_id_to_position(action_id)
            self.bot.move_location = new_position
            self.bot.table_data = self.bot.table_data + ("[bold]Current Action:[/bold]\t\t\t" + self.bot.action + " " + str(self.bot.move_location) + "\n\n")

            if not self.bot.in_pathing_grid(new_position):
                self.bot.reward = self.bot.reward - 0.025

            self.bot.do(self.bot.train_unit.move(new_position))

        elif action_id == 9:  # Attack
            enemies: Units = self.bot.all_units.filter(lambda unit: not unit.is_mine)
            if enemies:
                next_enemy = enemies.closest_to(self.bot.train_unit)
                self.bot.attack_position = Point2((int(next_enemy.position.x), int(next_enemy.position.y)))
                self.bot.table_data = self.bot.table_data + ("[bold]Current Action:[/bold]\t\t\t" + self.bot.action + " " + str(self.bot.attack_position) + "\n\n")
                if next_enemy:
                    self.bot.do(self.bot.train_unit.attack(next_enemy))
        elif action_id == 10:  # Ability
            enemies: Units = self.bot.all_units.filter(
                lambda unit: not unit.is_mine)

            if enemies:
                next_enemy = enemies.closest_to(self.bot.train_unit)
                if next_enemy:
                    unit_abilities = await self.bot.get_available_abilities(self.bot.train_unit)
                    if AbilityId.KD8CHARGE_KD8CHARGE in unit_abilities:
                        self.bot.attack_position = Point2((int(next_enemy.position.x), int(next_enemy.position.y)))
                        self.bot.table_data = self.bot.table_data + ("[bold]Current Action:[/bold]\t\t\t" + self.bot.action + " " + str(self.bot.attack_position) + "\n\n")
                        mine_position = self.bot.train_unit.position.towards(next_enemy.position, 5)
                        self.bot.do(self.bot.train_unit(AbilityId.KD8CHARGE_KD8CHARGE, mine_position))
        else:
            self.bot.table_data = self.bot.table_data + ("[bold]Current Action:[/bold]\t\t\t" + action_id + "\n\n")

    def translate_move_id_to_position(self, move_id):
        """
        Translates Move-IDs into coordinates
        """
        raw_unit_position = self.bot.train_unit.position
        unit_position = Point2((int(raw_unit_position.x), int(raw_unit_position.y)))

        if move_id == 0:  # Dont Move
            return Point2((unit_position.x, unit_position.y))
        elif move_id == 1:  # North
            return Point2((unit_position.x, unit_position.y + 1))
        elif move_id == 2:  # North East
            return Point2((unit_position.x + 1, unit_position.y + 1))
        elif move_id == 3:  # East
            return Point2((unit_position.x + 1, unit_position.y))
        elif move_id == 4:  # South East
            return Point2((unit_position.x + 1, unit_position.y - 1))
        elif move_id == 5:  # South
            return Point2((unit_position.x, unit_position.y - 1))
        elif move_id == 6:  # South West
            return Point2((unit_position.x - 1, unit_position.y - 1))
        elif move_id == 7:  # West
            return Point2((unit_position.x - 1, unit_position.y))
        elif move_id == 8:  # North West
            return Point2((unit_position.x - 1, unit_position.y + 1))