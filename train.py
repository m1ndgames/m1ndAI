from typing import Dict, Callable
from sc2.units import Units
from sc2.position import Point2
import sc2
from agents.random_agent import RandomAgent
from agents.a3c_agent_play import *
from helpers.actions import ActionHandler
from helpers.observation import ObservationHandler
from rich.console import *
from rich.table import *
console = Console()

agents: Dict[str, Callable[[int, int], BaseMLAgent]] = {
    "learning": lambda env_name, s, a, b: A3CAgent(env_name, s, a),
    "play": lambda env_name, s, a, log: PlayA3CAgent(env_name, s, a),
    "random": lambda env_name, s, a, b: RandomAgent(s, a)
}


def removekey(d, key):
    r = dict(d)
    del r[key]
    return r


def clear_screen():
    if os.name == 'nt':
        _ = os.system('cls')
    else:
        _ = os.system('clear')


class TrainAi(sc2.BotAI):
    agent: BaseMLAgent

    def __init__(self, agent: str = "learning"):
        super().__init__()
        self.observation_handler = ObservationHandler(self)
        self.action_handler = ActionHandler(self)

        self.pre_step_units = {}
        self.bot = self
        self.reward = 0
        self.action = None
        self.train_unit = None
        self.training = False
        self.action_space = None
        self.observation_space = None
        self.action_size = 11
        self.state_size = 794
        self.initialize_agent(agent)
        self.bot.table = None
        self.table_data = ""
        self.vision_map = ""
        self.killed_enemies = 0     # Counts the Episodes killed units

        # Disable fog of war
        self.no_fow = False         # Use Debug cmd to remove Fog of War

        # Print Map to CLI
        self.print_cli = True      # Show a Table in CLI
        self.print_map = True       # Show the Agent Vision in the Table (print_cli needs to be True)

    def initialize_agent(self, agent: str):
        if isinstance(agent, BaseMLAgent):
            self.agent = agent
        else:
            self.agent = agents[agent]("random", self.state_size, self.action_size, self)

    def save_unit(self, unit):
        self.pre_step_units[unit.tag] = unit

    def read_unit(self, unit):
        if unit.tag in self.pre_step_units:
            return self.pre_step_units[unit.tag]

    async def calculate_reward(self):
        """
        Calculate the Reward
        """
        # Enemy units
        for enemy_unit in self.enemy_units:
            pre_step_enemy_unit = self.read_unit(enemy_unit)
            if not pre_step_enemy_unit:
                self.save_unit(enemy_unit)
            else:
                if enemy_unit.health < pre_step_enemy_unit.health:
                    self.reward = self.reward + 0.5

                if enemy_unit.shield < pre_step_enemy_unit.shield:
                    self.reward = self.reward + 0.25

    async def run_agent(self):
        """
        Start training the agent
        """
        self.training = True

        action = self.agent.choose_action(np.asarray(self.observation_space), self.reward)
        self.action = action
        await self.action_handler.perform_action(action)
        await self.calculate_reward()

    async def emulate_commander(self):
        """
        Emulates the commander. The unit is moved around the map to find enemies,
        when it sees an enemy, then the actual training starts.
        """
        # Move the Clients camera to the Trained Unit
        await self.client.move_camera(self.train_unit.position)

        if self.enemy_units or self.enemy_structures:
            await self.run_agent()

    async def on_step(self, iteration):
        """
        Runs every game step
        """
        self.bot.table = Table(title="[bold magenta]Reaper Training[/bold magenta]", show_header=False,
                               header_style="bold magenta")
        self.bot.table.add_column("[bold magenta]Data[/bold magenta]", width=80)
        self.bot.table.add_column("[bold magenta]Vision Map[/bold magenta]")

        if iteration == 0:
            self._client.game_step = 4

            if self.no_fow:
                await self.client.debug_show_map()

        if iteration == 1:
            await self.client.quick_save()

        if iteration > 0:
            enemies: Units = self.all_units.filter(lambda u: not u.is_mine)
            if not enemies:
                if self.training:
                    self.table_data = self.table_data + ("[bold]Episode end (no enemy units), reward:[/bold]\t" + str(self.reward) + "\n")
                    self.training = False
                    self.agent.end_round(self.observation_space, self.reward)
                    self.reward = 0
                    self.pre_step_units = {}
                    self.killed_enemies = 0

                await self.client.quick_load()

        # Defines the Unit to be trained
        if self.units(sc2.UnitTypeId.REAPER).ready:
            self.train_unit = self.units(sc2.UnitTypeId.REAPER).ready.first
        else:
            self.train_unit = None
            return

        self.observation_space = self.observation_handler.create_map(self.train_unit)
        if not self.action_space:
            self.action_space = self.action_handler.create_actions()

        if self.train_unit:
            await self.emulate_commander()

        self.pre_step_units = {}
        for unit in self.all_units:
            self.save_unit(unit)

        self.table_data = self.table_data + ("[bold]Episode - Reward:[/bold]\t\t" + str(self.reward) + "\n")
        self.table_data = self.table_data + ("[bold]Episode - Enemies Killed:[/bold]\t" + str(self.killed_enemies) + "\n")

        if self.print_cli:
            clear_screen()

            if self.print_map:
                self.bot.table.add_row(self.table_data, str(self.vision_map))
            else:
                self.bot.table.add_row(self.table_data, "Map Disabled")

            console.print(self.bot.table)
            self.bot.table_data = ""
            self.vision_map = ""

    async def on_unit_destroyed(self, unit_tag):
        """
        Gets called when a unit is destroyed.
        The current round ends and the agent is stopped.
        """
        if self.training:
            if unit_tag in self.pre_step_units:
                unit = self.pre_step_units[unit_tag]
                if unit.is_mine and unit.name == 'Reaper':

                    self.table_data = self.table_data + ("[bold]Episode end (unit died), reward:[/bold]\t" + str(self.reward) + "\n")

                    self.training = False
                    self.agent.end_round(self.observation_space, self.reward)
                    self.reward = 0
                    self.killed_enemies = 0
                    self.pre_step_units = {}
                    await self.client.quick_load()

                else:
                    self.pre_step_units = removekey(self.pre_step_units, unit_tag)
                    if not unit.is_mine:
                        self.killed_enemies = self.killed_enemies + 1

                        # Gives extra reward for destroyed enemy units
                        self.reward = self.reward + 1
